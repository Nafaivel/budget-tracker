create table if not exists currencies(
             id integer primary key AUTOINCREMENT,
             name text not null,
             symbol text,
             iso_code text
        );
create table if not exists accounts(
             id integer primary key AUTOINCREMENT,
             name text not null,
             currency integer not null references currencies(id),
             quantity integer
         );
create table if not exists categories(
             id integer primary key AUTOINCREMENT,
             name text not null,
             icon_text text,
             icon text,
             spend integer
        );
create table if not exists history(
             id integer primary key AUTOINCREMENT,
             account_id integer not null references accounts(id),
             category integer references categories(id),
             trans_type text not null,
             cval integer not null,
             comment text 
        );
