use crossterm::{
    event::{self, Event as CEvent},
    execute,
    terminal::{enable_raw_mode, EnterAlternateScreen},
};
use rusqlite::Connection;
use std::io;
use std::sync::mpsc;
use std::thread;
use std::time::{Duration, Instant};
use tui::widgets::ListState;

mod keybindings;
mod settings;
mod ui;

use keybindings::keybindings;
use settings::Settings;

use crate::db::Account;

pub enum Event<I> {
    Input(I),
    Tick,
}

pub enum TransType {
    Plus,
    Minus,
}

pub fn start() -> Result<(), Box<dyn std::error::Error>> {
    enable_raw_mode().expect("can run in raw mode");
    execute!(io::stdout(), EnterAlternateScreen)?;

    let (tx, rx) = mpsc::channel();
    let tick_rate = Duration::from_millis(200);
    thread::spawn(move || {
        let mut last_tick = Instant::now();
        loop {
            let timeout = tick_rate
                .checked_sub(last_tick.elapsed())
                .unwrap_or_else(|| Duration::from_secs(0));

            if event::poll(timeout).expect("poll works") {
                if let CEvent::Key(key) = event::read().expect("can read events") {
                    tx.send(Event::Input(key)).expect("can send events");
                }
            }

            if last_tick.elapsed() >= tick_rate {
                if let Ok(_) = tx.send(Event::Tick) {
                    last_tick = Instant::now();
                }
            }
        }
    });

    let mut app = App::default().unwrap();

    keybindings(rx, &mut app).expect("keybindings error");

    Ok(())
}

pub struct App {
    need_to_quit: bool,
    input_mode: InputMode,
    settings: Settings,
    selected_menu: MenuItem,
    accounts_menu: AccountMenuState,
    history_menu: HistoryMenuState,
    category_menu: CategoryMenuState,
    add_menu: AddMenuState,
    conn: Connection,
    prev_menu: Option<MenuItem>,
}

impl App {
    fn default() -> Result<(App), Box<dyn std::error::Error>> {
        let settings = Settings::get_settings();
        let conn_path = settings.get_db_path();
        if !conn_path.exists() {
            panic!()
        }
        let conn = Connection::open(conn_path).expect("failed to connect");

        Ok(App {
            need_to_quit: false,
            input_mode: InputMode::Normal,
            selected_menu: MenuItem::Summary,
            settings: Settings::get_settings(),
            add_menu: AddMenuState::new(),
            accounts_menu: AccountMenuState::new(&conn),
            history_menu: HistoryMenuState::new(),
            category_menu: CategoryMenuState::new(),
            conn,
            prev_menu: None,
        })
    }

    // returns true if all ok
    fn out_of_bound_check(checked: usize, arrlen: usize, add: i8) -> bool {
        if !((checked as i8 + add >= arrlen as i8) || (checked as i8 + add < 0)) {
            true
        } else {
            false
        }
    }

    pub fn list_move(&mut self, add: i8) {
        if self.input_mode != InputMode::Normal {
            match self.input_mode {
                _ => {}
            }
        } else {
            match self.selected_menu {
                MenuItem::Accounts => {
                    // out of bounds check
                    if Self::out_of_bound_check(
                        self.accounts_menu.pos.selected().unwrap(),
                        self.accounts_menu.accounts.len(),
                        add,
                    ) {
                        self.accounts_menu.pos.select(Some(
                            (self.accounts_menu.pos.selected().unwrap() as i8 + add) as usize,
                        ));
                    }
                }
                _ => {}
            }
        }
    }

    pub fn all_update(&mut self) {
        self.category_menu.update();
        self.history_menu.update();
        self.accounts_menu.update();
        self.add_menu.update();
    }

    pub fn quit(&mut self) {
        self.need_to_quit = true;
    }

    pub fn need_to_quit(&self) -> bool {
        self.need_to_quit
    }

    pub fn submit_add(&mut self) {
        self.add_menu.submit_add();
        self.return_to_prev_menu();
    }

    pub fn return_to_prev_menu(&mut self) {
        if self.prev_menu.is_some() {
            self.selected_menu = self.prev_menu.unwrap();
            self.prev_menu = None;
        }
    }

    pub fn go_add(&mut self, addmode: AddMode) {
        self.prev_menu = Some(self.selected_menu);
        self.selected_menu = MenuItem::Add;
        self.add_menu.addmode = addmode;
    }
}

#[derive(PartialEq)]
enum InputMode {
    Normal,
    Editing,
    Confirm,
    Help,
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub enum MenuItem {
    Summary,
    Accounts,
    History,
    Category,
    Add,
}

impl MenuItem {
    fn is_add(&self) -> bool {
        if self == &MenuItem::Add {
            true
        } else {
            false
        }
    }
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub enum AddMode {
    Plus,
    Minus,
    Account,
    Currency,
    Category,
    None,
}

impl AddMode {
    fn is_plus(&self) -> bool {
        if self == &AddMode::Plus {
            true
        } else {
            false
        }
    }
    fn is_minus(&self) -> bool {
        if self == &AddMode::Minus {
            true
        } else {
            false
        }
    }
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub struct AddMenuState {
    addmode: AddMode,
}

impl AddMenuState {
    fn new() -> AddMenuState {
        AddMenuState {
            addmode: AddMode::None,
        }
    }

    fn clear(&mut self) {
        self.addmode = AddMode::None;
    }

    fn submit_add(&mut self) {
        match self.addmode {
            AddMode::Plus => {
                // TODO: add note action
                self.clear()
            }
            AddMode::Minus => {}
            _ => {}
        }
    }

    fn update(&mut self) {}
}

#[derive(Clone, Debug)]
pub struct AccountMenuState {
    accounts: Vec<Account>,
    pos: ListState,
}

impl AccountMenuState {
    fn new(conn: &Connection) -> AccountMenuState {
        let mut pos = ListState::default();
        pos.select(Some(0));
        AccountMenuState {
            accounts: Account::get_all_from_db(conn).expect("failed to get accs"),
            pos,
        }
    }

    fn update(&mut self) {}
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub struct CurrencyMenuState {}

impl CurrencyMenuState {
    fn new() -> CurrencyMenuState {
        CurrencyMenuState {}
    }

    fn update(&mut self) {}
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub struct CategoryMenuState {}

impl CategoryMenuState {
    fn new() -> CategoryMenuState {
        CategoryMenuState {}
    }

    fn update(&mut self) {}
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub struct HistoryMenuState {}

impl HistoryMenuState {
    fn new() -> HistoryMenuState {
        HistoryMenuState {}
    }

    fn update(&mut self) {}
}
