use druid::widget::{Button, Flex, Label};
use druid::{im::Vector, Data, Env, EventCtx, Lens, Size, Widget, WindowDesc};
use rusqlite::Connection;

use crate::druid::delegate::{DELETE_ACC, DELETE_CAT, DELETE_CURR};

use crate::db::{db_create, Account, Category, Currency, HistEntry};

#[derive(Clone, Data, Lens)]
pub struct AppState {
    accounts: Vector<ListItem>,
    categories: Vector<ListItem>,
    currencies: Vector<ListItem>,
    history: Vector<ListItem>,
    conn_path: String,
}

impl AppState {
    pub fn new(
        accs: Vec<ListItem>,
        cats: Vec<ListItem>,
        curr: Vec<ListItem>,
        hist: Vec<ListItem>,
        conn_path: String,
    ) -> Self {
        Self {
            accounts: Vector::from(accs),
            categories: Vector::from(cats),
            currencies: Vector::from(curr),
            history: Vector::from(hist),
            conn_path,
        }
    }

    // fn create_acc(&mut self) {

    // }

    pub fn click_acc_create(ctx: &mut EventCtx, _data: &mut Self, _env: &Env) {
        let window_add = WindowDesc::new(AppState::build_add_ui);
        ctx.new_window(window_add);
    }

    fn build_add_ui() -> impl Widget<AppState> {
        Flex::column()
            .with_child(Label::new("are you shure?"))
            .with_child(
                Flex::row()
                    .with_child(Button::new("ok"))
                    .with_child(Button::new("no")),
            )
    }

    pub fn delete_acc(&mut self, id: &i32) {
        self.accounts.retain(|item| &item.id != id);
        let conn = Connection::open(&self.conn_path).unwrap();
        Account::rm_by_id(id.clone(), &conn).unwrap();
    }

    pub fn delete_curr(&mut self, id: &i32) {
        self.currencies.retain(|item| &item.id != id);
        let conn = Connection::open(&self.conn_path).unwrap();
        Currency::rm_by_id(id.clone(), &conn).unwrap();
    }

    pub fn delete_cat(&mut self, id: &i32) {
        self.categories.retain(|item| &item.id != id);
        let conn = Connection::open(&self.conn_path).unwrap();
        Category::rm_by_id(id.clone(), &conn).unwrap();
    }

    pub fn db_open(&mut self) {
        let conn = Connection::open(&self.conn_path).unwrap();
        self.categories.retain(|_item| true);
        self.accounts.retain(|_item| true);
        self.currencies.retain(|_item| true);
        let accounts: Vec<Account> = Account::get_all_from_db(&conn).unwrap();
        let currencies: Vec<Currency> = Currency::get_all_from_db(&conn).unwrap();
        let categories: Vec<Category> = Category::get_all_from_db(&conn).unwrap();

        for acc in accounts {
            self.accounts.push_back(ListItem::new(
                &format!(
                    "{} {} {}",
                    &acc.name, &acc.quantity, &currencies[acc.currency].iso_code
                ),
                acc.id.unwrap(),
            ));
        }
        for cat in categories {
            self.categories.push_back(ListItem::new(
                &format!("{} {} {}", &cat.icon_text, &cat.name, &cat.spend),
                cat.id.unwrap(),
            ));
        }
        for curr in currencies {
            self.currencies.push_back(ListItem::new(
                &format!("{} {} {}", curr.iso_code, curr.symbol, curr.name),
                curr.id.unwrap(),
            ));
        }
        self.history = Vector::from(list_hist(&conn));
    }

    pub fn click_db_open(_ctx: &mut EventCtx, data: &mut Self, _env: &Env) {
        data.db_open()
    }

    pub fn db_create(&mut self) {
        let mut conn = Connection::open(&self.conn_path).unwrap();
        db_create(&mut conn).unwrap();
        self.categories.retain(|_item| true);
        self.accounts.retain(|_item| true);
        self.currencies.retain(|_item| true);
        let accounts: Vec<Account> = Account::get_all_from_db(&conn).unwrap();
        let currencies: Vec<Currency> = Currency::get_all_from_db(&conn).unwrap();
        let categories: Vec<Category> = Category::get_all_from_db(&conn).unwrap();

        for acc in accounts {
            self.accounts.push_back(ListItem::new(
                &format!(
                    "{} {} {}",
                    &acc.name, &acc.quantity, &currencies[acc.currency].iso_code
                ),
                acc.id.unwrap(),
            ));
        }
        for cat in categories {
            self.categories.push_back(ListItem::new(
                &format!("{} {} {}", &cat.icon_text, &cat.name, &cat.spend),
                cat.id.unwrap(),
            ));
        }
        for curr in currencies {
            self.currencies.push_back(ListItem::new(
                &format!("{} {} {}", curr.iso_code, curr.symbol, curr.name),
                curr.id.unwrap(),
            ));
        }
        self.history = Vector::from(list_hist(&conn));
    }

    pub fn click_db_create(_ctx: &mut EventCtx, data: &mut Self, _env: &Env) {
        data.db_create()
    }
}

#[derive(Clone, Data, Lens)]
pub struct ListItem {
    id: i32,
    text: String,
}

impl ListItem {
    pub fn new(text: &String, id: i32) -> Self {
        Self {
            id: id,
            text: text.into(),
        }
    }

    pub fn acc_click_rm(ctx: &mut EventCtx, data: &mut Self, _env: &Env) {
        ctx.submit_command(DELETE_ACC.with(data.id));
    }

    pub fn cat_click_rm(ctx: &mut EventCtx, data: &mut Self, _env: &Env) {
        ctx.submit_command(DELETE_CAT.with(data.id));
    }

    pub fn curr_click_rm(ctx: &mut EventCtx, data: &mut Self, _env: &Env) {
        ctx.submit_command(DELETE_CURR.with(data.id));
    }
}

pub fn list_hist(conn: &Connection) -> Vec<ListItem> {
    let mut stmt = conn
        .prepare("SELECT id, account_id, category, trans_type, cval, comment FROM history")
        .unwrap();
    let entry_iter = stmt
        .query_map([], |row| {
            let id: i32 = row.get(0)?;
            let account_id: i32 = row.get(1)?;
            let category: i32 = row.get(2)?;
            let trans_type: String = row.get(3)?;
            let cval: i64 = row.get(4)?;
            let comment: String = row.get(5)?;
            Ok(HistEntry::new_from_db(
                id, account_id, category, trans_type, cval, comment,
            ))
        })
        .unwrap();
    let mut history: Vec<ListItem> = vec![];
    for entry in entry_iter {
        /* println!("{:?}", entry); */
        let uentry = entry.unwrap();
        history.push(ListItem::new(
            &format!(
                "{}{}, {}, id:{}, acc_id:{}  {}",
                uentry.trans_type,
                uentry.cval,
                uentry.category,
                uentry.id,
                uentry.account_id,
                uentry.comment
            ),
            uentry.id,
        ));
    }
    history
}
