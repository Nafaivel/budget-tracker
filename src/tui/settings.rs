use std::collections::hash_map::HashMap;
use std::path::PathBuf;

pub struct Settings {
    path2notes: String,
    langmaps: HashMap<String, String>,
}

impl Settings {
    pub fn get_settings() -> Settings {
        Settings {
            path2notes: "~/development/rust/budget-tracker/base.db".to_string(),
            langmaps: parse_langmaps(),
        }
    }

    pub fn get_db_path(&self) -> PathBuf {
        let mut notes = PathBuf::new();
        for part in self.path2notes.split("/") {
            if part == "~" {
                notes.push(dirs::home_dir().unwrap());
            } else {
                notes.push(part);
            }
        }
        notes
    }

    pub fn get_langmaps(&self) -> HashMap<String, String> {
        self.langmaps.clone()
    }
}

fn parse_langmaps() -> HashMap<String, String> {
    HashMap::from([
        (
            "en".to_string(),
            "qwertyuiop[]asdfghjkl;'zxcvbnm,./QWERTYUIOP{}ASDFGHJKL:\"ZXCVBNM<>?".to_string(),
        ),
        (
            "be".to_string(),
            "йцукенгшўзх'фывапролджэячсмітьбю.ЙЦУКЕНГШЎЗХ'ФЫВАПРОЛДЖЭЯЧСМІТЬБЮ,".to_string(),
        ),
        // (
        //     "ru".to_string(),
        //     "йцукенгшщзхъфывапролджэячсмитьбю. ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ,".to_string(),
        // ),
    ])
}

