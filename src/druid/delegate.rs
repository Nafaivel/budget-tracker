use druid::{AppDelegate, Command, DelegateCtx, Env, Handled, Selector, Target};


use crate::druid::data::AppState;

pub const DELETE_ACC: Selector<i32> = Selector::new("acc.delete");
pub const DELETE_CAT: Selector<i32> = Selector::new("cat.delete");
pub const DELETE_CURR: Selector<i32> = Selector::new("curr.delete");

pub struct Delegate;

impl AppDelegate<AppState> for Delegate {
    fn command(
        &mut self,
        _ctx: &mut DelegateCtx,
        _target: Target,
        cmd: &Command,
        data: &mut AppState,
        _env: &Env,
    ) -> Handled {
        if let Some(id) = cmd.get(DELETE_ACC) {
            data.delete_acc(id);
            Handled::Yes
        }
        else if let Some(id) = cmd.get(DELETE_CAT) {
            data.delete_cat(id);
            Handled::Yes
        }
        else if let Some(id) = cmd.get(DELETE_CURR) {
            data.delete_curr(id);
            Handled::Yes
        }
        else {
            println!("cmd forwarded: {:?}", cmd);
            Handled::No
        }
    }
}