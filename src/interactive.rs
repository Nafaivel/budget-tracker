use crate::db::*;
use rusqlite::Connection;
use std::io::{self,Write};

//main interactive function
pub fn start(){
    clear();
    // create connection
    println!("print path");
    let path = writl();
    let conn = new_db_conn(&path);
    let mut accounts:Vec<Account> = Account::get_all_from_db(&conn).unwrap();
    let mut currencies:Vec<Currency> = Currency::get_all_from_db(&conn).unwrap();
    let mut categories:Vec<Category> = Category::get_all_from_db(&conn).unwrap();
    clear();

    // go to program loop
    print_hello();
    loop{
        let option:String = writl();
        if (option == "exit") | (option =="q") {
            break
        }
        else if option == "help"{
            help()
        }
        else if (option == "new acc") | (option == "new account"){
            println!("print account name");
            let acc_name:String = writl();
            list_curr(&currencies);
            println!("print number of currency");
            let currency = writl().trim().parse::<usize>().unwrap_or(1);
            println!("print quantity of mouney on account");
            let quantity:i64 = writl().trim().parse::<i64>().unwrap();
            let mut acc = Account::new(None, acc_name, currency, Some(quantity));
            acc.add_to_db(&conn).unwrap();
            println!("{}",conn.last_insert_rowid());
            acc.id = Some(conn.last_insert_rowid() as i32);
            accounts.push(acc);
        }
        else if (option == "new curr") | (option == "new currency") {
            println!("print currency name");
            let name:String = writl();
            println!("print currency symbol");
            let symbol:String = writl();
            println!("print currensy ISO code (usualy contains 3 uppercase symbols)");
            let iso_code:String = writl();
            let curr = Currency::new(name, symbol, iso_code);
            curr.add_to_db(&conn).unwrap();
            currencies.push(curr);
        }
        else if (option == "new cat") | (option == "new category"){
            println!("print category name");
            let name:String = writl();
            println!("print text icon(find in symbolic fonts)");
            let icon_text:String = writl();
            let icon_path= None;
            
            let cat = Category::new(name, icon_text, icon_path);
            cat.add_to_db(&conn).unwrap();
        }
        else if (option == "rm acc") | (option == "rm account"){
            list_acc(&accounts, &currencies);
            println!("print account to remove id");
            let id:usize = writl().parse::<usize>().expect("wrong input");
            accounts[id-1].rm(&conn).unwrap();
            accounts.remove(id-1);
        }
        else if (option == "rm curr") | (option == "rm currecy"){
            list_curr(&currencies);
            println!("print currency to remove id");
            let id:usize = writl().parse::<usize>().expect("wrong input");
            currencies[id-1].rm(&conn, &accounts).unwrap();
            currencies.remove(id-1);
        }
        else if (option == "rm cat") | (option == "rm category"){
            list_cat(&categories);
            println!("print category to remove id");
            let id:usize = writl().parse::<usize>().expect("wrong input");
            categories[id-1].rm(&conn).unwrap();
            categories.remove(id-1);
        }
        else if (option == "sub") | (option == "-"){
            list_acc(&accounts, &currencies);
            println!("print account id");
            let acc_id:usize = writl().parse::<usize>().expect("wrong input");
            println!("print value");
            let value:i64 = writl().parse::<i64>().expect("wrong input");
            list_cat(&categories);
            println!("print category id");
            let cat_id:usize = writl().parse::<usize>().expect("wrong input");
            println!("print comment");
            let comment:String = writl();
            Account::sub(&conn, &mut accounts[acc_id-1], &mut categories[cat_id-1], value, Some(comment)).unwrap();
        }
        else if (option == "add") | (option == "+"){
            list_acc(&accounts, &currencies);
            println!("print account id");
            let acc_id:usize = writl().parse::<usize>().expect("wrong input");
            println!("print value");
            let value:i64 = writl().parse::<i64>().expect("wrong input");
            println!("print comment");
            let comment:String = writl();
            Account::add(&conn, &mut accounts[acc_id-1], value, Some(comment)).unwrap();
        }
        else if (option == "list acc") | (option == "list accounts") {
            list_acc(&accounts, &currencies)
        }
        else if (option == "list curr") | (option == "list currencies"){
            list_curr(&currencies)
        }
        else if (option == "list hist") | (option == "list history") {
            list_hist(&conn);
        }
        else if (option == "list cat") | (option == "list categories"){
            list_cat(&categories)
        }
    }
}

fn clear(){
    if cfg!(windows){
        std::process::Command::new("cls").status().unwrap();
    }
    else{
        std::process::Command::new("clear").status().unwrap();
    }
}

fn list_hist(conn:&Connection){
    let mut stmt =conn.prepare("SELECT id, account_id, category, trans_type, cval, comment FROM history").unwrap();
    let entry_iter = stmt.query_map([], |row|{
        let id: i32 = row.get(0)?;
        let account_id: i32 = row.get(1)?;
        let category: i32 = row.get(2)?;
        let trans_type: String = row.get(3)?;
        let cval : i64 = row.get(4)?;
        let comment : String = row.get(5)?;
        Ok(HistEntry::new_from_db(id, account_id, category, trans_type, cval, comment))
    }).unwrap();
    for entry in entry_iter {
        /* println!("{:?}", entry); */
        let uentry = entry.unwrap(); 
        println!("{}{}, {}, id:{}, acc_id:{}  {}", uentry.trans_type, uentry.cval, uentry.category, uentry.id, uentry.account_id, uentry.comment);
    } 
}
fn list_acc(accounts:&Vec<Account>, currencies:&Vec<Currency>){
    let mut counter = 1;
    for account in accounts{
        println!("{} {} {} {}", counter, account.name, account.quantity, currencies[account.currency].iso_code);
        counter+=1;
    }
}
fn list_curr(currencies:&Vec<Currency>){
    let mut counter = 1;
    for curr in currencies{
        println!("{} {} {} {}", counter, curr.symbol, curr.iso_code, curr.name);
        counter+=1;
    }
}
fn list_cat(categories:&Vec<Category>){
    let mut counter = 1;
    for cat in categories{
        println!("{} {} {} {}", counter, cat.icon_text, cat.spend, cat.name);
        counter+=1;
    }
}

fn prompt(){
    print!(">> ");
    io::stdout().flush().unwrap();
}
fn help(){
    clear();
    println!("\
open - open db
list acc - print list of existing accounts
list curr - print list of existing currency
new acc - create new account
new curr - create new currency
help - print this message
exit - exit from program")
}

fn print_hello(){
    println!("\
help - print all help
exit - exit from program
q - exit from program")
}

fn writl()->String{
    prompt();
    let mut st = String::new();
    io::stdin().read_line(&mut st)
    .expect("Failed to read line");
    st = st.trim_end_matches("\n").to_string();
    st
}

fn new_db_conn(db_path:&String)-> Connection{

    if std::path::Path::new(db_path).exists(){
        clear();
    }
    else{
        println!("not exists");
        println!("type \"yes\" if want create db");
        let ask:String = writl();
        if ask =="yes"{
            let mut conn = Connection::open(db_path).unwrap();
            db_create(&mut conn).unwrap();
            println!("db created");
        }
        else{
            println!("programm will terminate with Error")
        }
    }
    let conn = Connection::open(db_path).unwrap();
    return conn
}
