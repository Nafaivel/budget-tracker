use crossterm::{
    event::{KeyCode, KeyEvent},
    execute,
    terminal::{disable_raw_mode, LeaveAlternateScreen},
};
use std::io;
use std::sync::mpsc::Receiver;
// use std::{fs, path::PathBuf};
use tui::{backend::CrosstermBackend, Terminal};
use unicode_segmentation::UnicodeSegmentation;

use crate::tui::{ui::ui, AddMode, App, Event, InputMode};

use super::MenuItem;

pub fn keybindings(
    rx: Receiver<Event<KeyEvent>>,
    app: &mut App,
) -> Result<(), Box<dyn std::error::Error>> {
    let stdout = io::stdout();
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;
    terminal.clear()?;

    loop {
        terminal.draw(|f| ui(f, app))?;

        if app.need_to_quit() {
            disable_raw_mode()?;
            terminal.show_cursor()?;
            execute!(io::stdout(), LeaveAlternateScreen)?;
            break;
        }

        match rx.recv()? {
            Event::Input(event) => match app.input_mode {
                InputMode::Editing => match_editing_keys(app, event.code),
                InputMode::Normal => match_normal_keys(app, event.code),
                InputMode::Confirm => match_confirm_keys(app, event.code),
                _ => todo!("new input mode must be here"),
            },
            Event::Tick => {}
        }
    }
    Ok(())
}

// HACK: think it can be writed better
fn find_char_pos(s: &str, c: char) -> Option<usize> {
    let mut counter = 0;
    for (i, grapheme) in s.grapheme_indices(true) {
        if grapheme == c.to_string() {
            return Some(counter);
        }
        counter += 1;
    }
    None
}

/// converts non-english char to english with help of keymaps
fn match_key_char(app: &App, key: char) -> char {
    let langmaps = app.settings.get_langmaps();
    if langmaps["en"].contains(key) {
        return key;
    } else {
        for (_name, langmap) in &langmaps {
            if let Some(pos) = find_char_pos(langmap, key) {
                // dbg!(find.unwrap() / 2);
                return langmaps["en"]
                    .chars()
                    // Don't know how it works but find.unwrap returns result of find x2
                    .nth(pos)
                    .expect("there always be element if langmap correct");
                // return ' ';
            }
        }
        return ' ';
    }
}

// TODO: add keybindings
fn match_normal_keys(app: &mut App, code: KeyCode) {
    match code {
        KeyCode::Char(chr) => match match_key_char(app, chr) {
            'q' => app.quit(),
            'u' => app.all_update(),
            'z' => app.selected_menu = MenuItem::Summary,
            'x' => app.selected_menu = MenuItem::Accounts,
            'c' => app.selected_menu = MenuItem::History,
            'v' => app.selected_menu = MenuItem::Category,
            'a' => app.go_add(AddMode::Plus),
            's' => app.go_add(AddMode::Minus),
            'j' => app.list_move(1),
            'k' => app.list_move(-1),
            _ => {}
        },
        KeyCode::Enter => {
            if app.selected_menu.is_add() {
                app.submit_add()
            }
        }
        KeyCode::Esc => {
            if app.selected_menu.is_add() {
                app.return_to_prev_menu()
            }
        }
        _ => {}
    }
}

fn match_editing_keys(app: &mut App, code: KeyCode) {}

fn match_confirm_keys(app: &mut App, code: KeyCode) {}
