pub mod cli;
pub mod interactive;
// pub mod gtk;
pub mod druid;
pub mod tui;
pub mod db;

use std::env;

fn main(){
    let args: Vec<String> = env::args().collect();
    // gtk::start();
    if args.len() > 1 {
        if args[1] == "cli"{
            cli::get_args(args);
        }
        else if args[1] == "interactive"{
            interactive::start()
        }
        else if args[1] == "gtk"{
            // gtk::start();
        }
        else if args[1] == "druid"{
            druid::start();
        }
        else if args[1] == "tui"{
            tui::start().unwrap();
        }
    }
    else{
        tui::start().unwrap();
        // println!("run programm with \"interactive\" arg");
    }
}
