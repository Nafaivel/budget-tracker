// use crate::db::*;
use rusqlite::Connection;

//main clio function
pub fn get_args(args:Vec<String>){
    for argument in 2..args.len(){
        if args[argument]== "--help"{
            help();
        }
        else if args[argument] == "--db"{
            new_db_conn(&args[argument+1]);
        }
    }
}

fn help(){
    println!("help message")
}
fn new_db_conn(db_path:&String)-> Connection{

    if std::path::Path::new(db_path).exists(){
        println!("exists")
    }
    else{
        println!("not exists")
    }
    let conn = Connection::open(db_path).unwrap();
    return conn
}