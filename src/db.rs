use std::fs::File;
use std::io::prelude::*;
use rusqlite::{Connection, Result};

pub fn db_create(conn:&mut Connection) -> Result<()> {
    //available_currencies content name, symbol, iso_code
    // let avail_curr = [
    // vec!["Russian ruble", "Afghan afghani", "Euro", "Albanian lek", "British pound[F]", "Guernsey pound", "Algerian dinar", "Euro", "Angolan kwanza", "Eastern Caribbean dollar", "Eastern Caribbean dollar", "Argentine peso", "Armenian dram", "Armenian dram", "Aruban florin", "Saint Helena pound", "Australian dollar", "Euro", "Azerbaijani manat", "Bahamian dollar", "Bahraini dinar", "Bangladeshi taka", "Barbadian dollar", "Belarusian ruble", "Euro", "Belize dollar", "West African CFA franc", "Bermudian dollar", "Bhutanese ngultrum", "Indian rupee", "Bolivian boliviano", "United States dollar[H]", "Bosnia and Herzegovina convertible mark", "Botswana pula", "Brazilian real", "United States dollar", "United States dollar", "Brunei dollar", "Singapore dollar", "Bulgarian lev", "West African CFA franc", "Burundian franc", "Cambodian riel", "Central African CFA franc", "Canadian dollar", "Cape Verdean escudo", "Cayman Islands dollar", "Central African CFA franc", "Central African CFA franc", "Chilean peso", "Chinese yuan", "Colombian peso", "Comorian franc", "Congolese franc", "Central African CFA franc", "Cook Islands dollar", "New Zealand dollar", "Costa Rican colón", "West African CFA franc", "Croatian kuna", "Cuban peso", "Netherlands Antillean guilder", "Euro", "Czech koruna", "Danish krone", "Djiboutian franc", "Eastern Caribbean dollar", "Dominican peso", "United States dollar", "United States dollar", "Egyptian pound", "United States dollar", "Bitcoin[5]", "Central African CFA franc", "Eritrean nakfa", "Euro", "Swazi lilangeni", "South African rand", "Ethiopian birr", "Falkland Islands pound", "Danish krone", "Faroese króna", "Fijian dollar", "Euro", "Euro", "CFP franc", "Central African CFA franc", "Gambian dalasi", "Georgian lari", "Euro", "Ghanaian cedi", "Gibraltar pound", "Euro", "Danish krone", "Eastern Caribbean dollar", "Guatemalan quetzal", "Guernsey pound", "British pound[F]", "Guinean franc", "West African CFA franc", "Guyanese dollar", "Haitian gourde", "Honduran lempira", "Hong Kong dollar", "Hungarian forint", "Icelandic króna", "Indian rupee", "Indonesian rupiah", "Iranian rial", "Iraqi dinar", "Euro", "Manx pound", "British pound[F]", "Israeli new shekel", "Euro", "Jamaican dollar", "Japanese yen", "Jersey pound", "British pound[F]", "Jordanian dinar", "Kazakhstani tenge", "Kenyan shilling", "Kiribati dollar[E]", "Australian dollar", "North Korean won", "South Korean won", "Euro", "Kuwaiti dinar", "Kyrgyzstani som", "Lao kip", "Euro", "Lebanese pound", "Lesotho loti", "South African rand", "Liberian dollar", "Libyan dinar", "Swiss franc", "Euro", "Euro", "Macanese pataca", "Hong Kong dollar", "Malagasy ariary", "Malawian kwacha", "Malaysian ringgit", "Maldivian rufiyaa", "West African CFA franc", "Euro", "United States dollar", "Mauritanian ouguiya", "Mauritian rupee", "Mexican peso", "United States dollar", "Moldovan leu", "Euro", "Mongolian tögrög", "Euro", "Eastern Caribbean dollar", "Moroccan dirham", "Mozambican metical", "Burmese kyat", "Namibian dollar", "South African rand", "Australian dollar", "Nepalese rupee", "Euro", "CFP franc", "New Zealand dollar", "Nicaraguan córdoba", "West African CFA franc", "Nigerian naira", "New Zealand dollar", "Macedonian denar", "Turkish lira", "Norwegian krone", "Omani rial", "Pakistani rupee", "United States dollar", "Israeli new shekel", "Jordanian dinar", "Panamanian balboa", "United States dollar", "Papua New Guinean kina", "Paraguayan guaraní", "Peruvian sol", "Philippine peso", "New Zealand dollar", "Pitcairn Islands dollar[E]", "Polish złoty", "Euro", "Qatari riyal", "Romanian leu", "Russian ruble", "Rwandan franc", "United States dollar[H]", "Algerian dinar", "Mauritanian ouguiya", "Moroccan dirham", "Saint Helena pound", "Eastern Caribbean dollar", "Eastern Caribbean dollar", "Eastern Caribbean dollar", "Samoan tālā", "Euro", "São Tomé and Príncipe dobra", "Saudi riyal", "West African CFA franc", "Serbian dinar", "Seychellois rupee", "Sierra Leonean leone", "Singapore dollar", "Brunei dollar", "United States dollar[H]", "Netherlands Antillean guilder", "Euro", "Euro", "Solomon Islands dollar", "Somali shilling", "Somaliland shilling", "South African rand", "Russian ruble", "Euro", "South Sudanese pound", "Sri Lankan rupee", "Sudanese pound", "Surinamese dollar", "Swedish krona", "Swiss franc", "Syrian pound", "New Taiwan dollar", "Tajikistani somoni", "Tanzanian shilling", "Thai baht", "West African CFA franc", "Tongan paʻanga[P]", "Transnistrian ruble", "Trinidad and Tobago dollar", "Tunisian dinar", "Turkish lira", "Turkmenistan manat", "United States dollar", "Tuvaluan dollar", "Australian dollar", "Ukrainian hryvnia", "United Arab Emirates dirham", "British pound[F]", "United States dollar", "Uruguayan peso", "Uzbekistani soʻm", "Vanuatu vatu", "Euro", "Venezuelan bolívar soberano", "Venezuelan bolívar digital", "Vietnamese đồng", "CFP franc", "Yemeni rial", "Zambian kwacha"],
    // vec!["₽", "؋", "€", "L", "£", "£", "د.ج", "€", "Kz", "$", "$", "$", "֏", "֏", "ƒ", "£", "$", "€", "₼", "$", ".د.ب", "৳", "$", "Br", "€", "$", "Fr", "$", "Nu.", "₹", "Bs.", "$", "KM or КМ[I]", "P", "R$", "$", "$", "$", "$", "лв.", "Fr", "Fr", "៛", "Fr", "$", "Esc or $", "$", "Fr", "Fr", "$", "¥ or 元", "$", "Fr", "Fr", "Fr", "$", "$", "₡", "Fr", "kn", "$", "ƒ", "€", "Kč", "kr", "Fr", "$", "RD$", "$", "$", "£ or ج.م", "$", "₿", "Fr", "Nfk", "€", "L", "R", "Br", "£", "kr", "kr", "$", "€", "€", "₣", "Fr", "D", "₾", "€", "₵", "£", "€", "kr", "$", "Q", "£", "£", "Fr", "Fr", "$", "G", "L", "$", "Ft", "kr", "₹", "Rp", "﷼", "ع.د", "€", "£", "£", "₪", "€", "$", "¥ or 円", "£", "£", "د.ا", "₸", "Sh", "$", "$", "₩", "₩", "€", "د.ك", "с", "₭", "€", "ل.ل", "L", "R", "$", "ل.د", "Fr.", "€", "€", "MOP$", "$", "Ar", "MK", "RM", ".ރ", "Fr", "€", "$", "UM", "₨", "$", "$", "L", "€", "₮", "€", "$", "د.م.", "MT", "Ks", "$", "R", "$", "रू", "€", "₣", "$", "C$", "Fr", "₦", "$", "ден", "₺", "kr", "ر.ع.", "₨", "$", "₪", "د.ا", "B/.", "$", "K", "₲", "S/.", "₱", "$", "$", "zł", "€", "ر.ق", "lei", "₽", "Fr", "$", "د.ج", "UM", "د. م.", "£", "$", "$", "$", "T", "€", "Db", "﷼", "Fr", "дин. or din.", "₨", "Le", "$", "$", "$", "ƒ", "€", "€", "$", "Sh", "Sl", "R", "₽", "€", "£", "Rs, රු or ரூ", "ج.س.", "$", "kr", "Fr.", "£ or ل.س", "$", "с.", "Sh", "฿", "Fr", "T$", "р.", "$", "د.ت", "₺", "m.", "$", "$", "$", "₴", "د.إ", "£", "$", "$", "Sʻ", "Vt", "€", "Bs.S. or Bs.", "Bs.", "₫", "₣", "﷼", "ZK"],
    // vec!["RUB", "AFN", "EUR", "ALL", "GBP", "GGP", "DZD", "EUR", "AOA", "XCD", "XCD", "ARS", "AMD", "AMD", "AWG", "SHP", "AUD", "EUR", "AZN", "BSD", "BHD", "BDT", "BBD", "BYN", "EUR", "BZD", "XOF", "BMD", "BTN", "INR", "BOB", "USD", "BAM", "BWP", "BRL", "USD", "USD", "BND", "SGD", "BGN", "XOF", "BIF", "KHR", "XAF", "CAD", "CVE", "KYD", "XAF", "XAF", "CLP", "CNY", "COP", "KMF", "CDF", "XAF", "CKD[G]", "NZD", "CRC", "XOF", "HRK", "CUP", "ANG", "EUR", "CZK", "DKK", "DJF", "XCD", "DOP", "USD", "USD", "EGP", "USD", "BTC", "XAF", "ERN", "EUR", "SZL", "ZAR", "ETB", "FKP", "DKK", "FOK[G]", "FJD", "EUR", "EUR", "XPF", "XAF", "GMD", "GEL", "EUR", "GHS", "GIP", "EUR", "DKK", "XCD", "GTQ", "GGP[G]", "GBP", "GNF", "XOF", "GYD", "HTG", "HNL", "HKD", "HUF", "ISK", "INR", "IDR", "IRR", "IQD", "EUR", "IMP[G]", "GBP", "ILS", "EUR", "JMD", "JPY", "JEP[G]", "GBP", "JOD", "KZT", "KES", "KID[G]", "AUD", "KPW", "KRW", "EUR", "KWD", "KGS", "LAK", "EUR", "LBP", "LSL", "ZAR", "LRD", "LYD", "CHF", "EUR", "EUR", "MOP", "HKD", "MGA", "MWK", "MYR", "MVR", "XOF", "EUR", "USD", "MRU", "MUR", "MXN", "USD", "MDL", "EUR", "MNT", "EUR", "XCD", "MAD", "MZN", "MMK", "NAD", "ZAR", "AUD", "NPR", "EUR", "XPF", "NZD", "NIO", "XOF", "NGN", "NZD", "MKD", "TRY", "NOK", "OMR", "PKR", "USD", "ILS", "JOD", "PAB", "USD", "PGK", "PYG", "PEN", "PHP", "NZD", "PND[G]", "PLN", "EUR", "QAR", "RON", "RUB", "RWF", "USD", "DZD", "MRU", "MAD", "SHP", "XCD", "XCD", "XCD", "WST", "EUR", "STN", "SAR", "XOF", "RSD", "SCR", "SLL", "SGD", "BND", "USD", "ANG", "EUR", "EUR", "SBD", "SOS", "SLS[G]", "ZAR", "RUB", "EUR", "SSP", "LKR", "SDG", "SRD", "SEK", "CHF", "SYP", "TWD", "TJS", "TZS", "THB", "XOF", "TOP", "PRB[G]", "TTD", "TND", "TRY", "TMT", "USD", "TVD[G]", "AUD", "UAH", "AED", "GBP", "USD", "UYU", "UZS", "VUV", "EUR", "VES", "VED", "VND", "XPF", "YER", "ZMW"],
    // ];

    // TODO: add description to account, category tables
    // TODO: add date for history table

    conn.execute_batch(
        "create table if not exists currencies(
             id integer primary key AUTOINCREMENT,
             name text not null,
             symbol text,
             iso_code text
        );
        create table if not exists accounts(
                     id integer primary key AUTOINCREMENT,
                     name text not null,
                     currency integer not null references currencies(id),
                     quantity integer
                 );
        create table if not exists categories(
                     id integer primary key AUTOINCREMENT,
                     name text not null,
                     icon_text text,
                     icon text,
                     spend integer
                );
        create table if not exists history(
                     id integer primary key AUTOINCREMENT,
                     account_id integer not null references accounts(id),
                     category integer references categories(id),
                     trans_type text not null,
                     cval integer not null,
                     comment text 
                );"
    ).unwrap(); 

    // let all_same_length = avail_curr
    //     .iter()
    //     .all(|ref v| v.len() == avail_curr[0].len());

    // if all_same_length {
    //     let tx = conn.transaction()?;
    //     for counter in 0..avail_curr[0].len(){
    //         tx.execute(
    //             "INSERT INTO currencies (name, symbol, iso_code) values (?1, ?2, ?3);",
    //             &[&avail_curr[0][counter].to_string(), &avail_curr[1][counter].to_string(), &avail_curr[2][counter].to_string()],
    //         )?;
    //     }
    //     tx.commit()?;
    // }
    // else{
    //     println!("Avail currancies not the same length. \nTry recreate database after editing one of vectors inside avail_curr");
    // }
    //
    // let categories = vec![
    //     vec!["eat", "health", "transport", "hobby", "etc"],
    //     vec!["﫱", "", "", "", ""],
    //     vec!["", "", "", "", ""],
    //     vec!["0", "0", "0", "0", "0"]
    // ];
    //
    // let tx = conn.transaction()?;
    // for counter in 0..categories[0].len(){
    //     tx.execute(
    //         "INSERT INTO categories (name, icon_text, spend, icon) values(?1, ?2, ?3, ?4);",
    //         &[&categories[0][counter].to_string(), &categories[1][counter].to_string(), &categories[3][counter].to_string(), &categories[2][counter].to_string()],
    //     )?;
    // }
    // tx.commit().unwrap();

    Ok(())
}

#[derive(Debug)]
pub struct Currency{
    pub id: Option<i32>,
    pub name: String,
    pub symbol : String,
    pub iso_code : String
} 

impl Currency{
    pub fn new(name:String, symbol:String,iso_code:String) -> Currency{
        Currency{
            id:None,
            name,
            symbol,
            iso_code
        }
    }
    pub fn add_to_db(&self, conn:&Connection) -> Result<()>{
        conn.execute(
            "INSERT INTO currenies (name, symbol, iso_code) values(?1, ?2, ?3);",
            &[&self.name.to_string(), &self.symbol.to_string(), &self.iso_code.to_string()],
        )?;
        
        Ok(())
    }
    fn new_from_db(id: i32, name: String, symbol:String , iso_code:String) -> Currency{
        Currency{
            id: Some(id),
            name,
            symbol,
            iso_code
        }
    }
    pub fn get_all_from_db(conn: &Connection) -> Result<Vec<Currency>>{
        let mut stmt =conn.prepare("SELECT id, name, symbol, iso_code FROM currencies")?;
        let account_iter = stmt.query_map([], |row|{
            let id: i32 = row.get(0)?;
            let name: String = row.get(1)?;
            let symbol: String= row.get(2)?;
            let iso_code: String = row.get(3)?;
            Ok(Currency::new_from_db(id, name, symbol, iso_code))
        })?;
        let mut currencies = Vec::new();
        for account in account_iter {
            // println!("{:?}",account);
            currencies.push(account.unwrap());
        }
        // println!("{:#?}",accounts);
        Ok(currencies)
    }
    pub fn rm(&self, conn:&Connection, accounts:&Vec<Account>) -> Result<()>{
        let mut start_ex:bool = true;
        for acc in accounts{
            if acc.id.unwrap() == self.id.unwrap(){
                start_ex = false;
                println!("we can`t do this account with id={} use this currency",acc.id.unwrap());
            }
        }
        if start_ex{
            conn.execute(
                "DELETE FROM currencies
                WHERE id = ?1;",
                &[&self.id.unwrap()],
            )?;
        }
        Ok(())
    }
    pub fn rm_by_id(id:i32, conn:&Connection) -> Result<()>{
        let mut start_ex:bool = true;
        let accounts = Account::get_all_from_db(&conn).unwrap();
        for acc in accounts{
            if acc.currency == id as usize{
                start_ex = false;
                println!("we can`t do this account with id={} use this currency",acc.id.unwrap());
            }
        }
        if start_ex{
            conn.execute(
                "DELETE FROM currencies
                WHERE id = ?1;",
                &[&id],
            )?;
        }
        Ok(())
    }
}

#[derive(Debug)]
pub struct Category{
    pub id: Option<i32>,
    pub name: String,
    pub icon_text: String,
    pub icon_path: Option<String>,
    pub spend: i64
}
impl Category{
    pub fn new(name:String, icon_text:String, icon_path:Option<String>) -> Category{
        Category{
            id: None,
            name,
            icon_text,
            icon_path,
            spend:0
        }
    }
    pub fn add_to_db(&self, conn:&Connection) -> Result<()>{
        conn.execute(
            "INSERT INTO categories (name, icon_text, icon, spend) values(?1, ?2, ?3, ?4);",
            &[&self.name.to_string(), &self.icon_text, &self.icon_path.clone().unwrap(), &self.spend.to_string()],
        )?;
        
        Ok(())
    }
    fn new_from_db(id: i32, name: String, icon_text:String, icon_path:Option<String>, spend:i64) -> Category{
        Category{
            id: Some(id),
            name,
            icon_text,
            icon_path,
            spend

        }
    }
    pub fn get_all_from_db(conn: &Connection) -> Result<Vec<Category>>{
        let mut stmt =conn.prepare("SELECT id, name, icon_text, icon, spend FROM categories")?;
        let account_iter = stmt.query_map([], |row|{
            let id: i32 = row.get(0)?;
            let name: String = row.get(1)?;
            let icon_text: String= row.get(2)?;
            let icon_path: String = row.get(3)?;
            let spend: i64 = row.get(4)?;
            Ok(Category::new_from_db(id, name, icon_text, Some(icon_path), spend))
        })?;
        let mut categories = Vec::new();
        for account in account_iter {
            println!("{:?}",account);
            categories.push(account.unwrap());
        }
        // println!("{:#?}",accounts);
        Ok(categories)
    }
    // removes catergory and set category in history = 1
    pub fn rm(&self, conn:&Connection) -> Result<()>{
        conn.execute(
            "UPDATE history
            SET category = 1
            WHERE category = ?1",
            &[&self.id.unwrap()],
        )?;
        conn.execute(
            "DELETE FROM categories
            WHERE id = ?1;",
            &[&self.id.unwrap()],
        )?;
        Ok(())
    }
    pub fn rm_by_id(id:i32, conn:&Connection) -> Result<()>{
        conn.execute(
            "UPDATE history
            SET category = 1
            WHERE category = ?1",
            &[&id],
        )?;
        conn.execute(
            "DELETE FROM categories
            WHERE id = ?1;",
            &[&id],
        )?;
        Ok(())
    }
}

#[derive(Debug)]
pub struct HistEntry{
    pub id: i32,
    pub account_id:i32,
    pub category:i32,
    pub trans_type:String,
    /// cval, cval is amount of spents
    pub cval:i64,
    pub comment:String
}

impl HistEntry{
    pub fn new_from_db(id:i32, account_id:i32, category:i32, trans_type:String, cval:i64, comment:String) -> HistEntry{
        HistEntry{
            id,
            account_id,
            category,
            trans_type,
            cval,
            comment
        }
    }
}

#[derive(Debug, Clone)]
pub struct Account{
    pub id: Option<i32>,
    pub name: String,
    pub currency: usize,
    pub quantity: i64
} 

impl Account{
    pub fn new(id:Option<i32>, acc_name:String, currency:usize, quantity_option:Option<i64>) -> Account{
        let quantity:i64 = quantity_option.unwrap_or(0);
        Account{
            id,
            name: acc_name,
            currency,
            quantity
        }
    }
    fn new_from_db(id: i32, name: String, currency:usize, quantity:i64) -> Account{
        Account{
            id: Some(id),
            name,
            currency,
            quantity

        }
    }
    pub fn rm(&self, conn:&Connection) -> Result<()>{
        conn.execute(
            "DELETE FROM history
            WHERE account_id = ?1",
            &[&self.id.unwrap()],
        )?;
        conn.execute(
            "DELETE FROM ACCOUNTS
            WHERE id = ?1", 
            &[&self.id.unwrap()],
        )?;
        Ok(())
    }
    pub fn rm_by_id(id:i32, conn:&Connection) -> Result<()>{
        conn.execute(
            "DELETE FROM history
            WHERE account_id = ?1",
            &[&id],
        )?;
        conn.execute(
            "DELETE FROM ACCOUNTS
            WHERE id = ?1", 
            &[&id],
        )?;
        Ok(())
    }
    pub fn edit(&self){

    }
    pub fn add_to_db(&self, conn:&Connection) -> Result<()>{
        conn.execute(
            "INSERT INTO accounts (name, currency, quantity) values(?1, ?2, ?3);",
            &[&self.name.to_string(), &self.currency.to_string(), &self.quantity.to_string()],
        )?;
        
        Ok(())
    }
    pub fn get_all_from_db(conn: &Connection) -> Result<Vec<Account>>{
        let mut stmt =conn.prepare("SELECT id, name, currency, quantity FROM accounts")?;
        let account_iter = stmt.query_map([], |row|{
            let id: i32 = row.get(0)?;
            let name: String = row.get(1)?;
            let currency: usize= row.get(2)?;
            let quantity: i64 = row.get(3)?;
            Ok(Account::new_from_db(id, name, currency, quantity))
        })?;
        let mut accounts = Vec::new();
        for account in account_iter {
            // println!("{:?}",account);
            accounts.push(account.unwrap());
        }
        // println!("{:#?}",accounts);
        Ok(accounts)
    }
    //transaction, what increase quantity in account and add it to history
    pub fn add(conn:&Connection, acc:&mut Account, value:i64, comment:Option<String>) ->Result<()>{
        acc.quantity+=value;
        let comment = comment.unwrap_or(String::from(""));
        conn.execute(
            "INSERT INTO history (account_id, category, cval, trans_type, comment) values(?1, ?2, ?3, \"+\", ?4)",
            &[&acc.id.unwrap().to_string(), &"0".to_string(), &value.to_string(), &comment],
        )?;
        conn.execute(
            "UPDATE accounts
            SET quantity = ?2
            WHERE id=?1",
            &[&acc.id.unwrap().to_string(),&acc.quantity.to_string()],
        )?;

        Ok(())
    }
    //transaction, what decrease quantity in account and add it to history
    pub fn sub(conn:&Connection, acc:&mut Account, category:&mut Category, value:i64, comment:Option<String>) ->Result<()>{
        acc.quantity-=value;
        category.spend+=value;
        let comment = comment.unwrap_or(String::from(""));
        conn.execute(
            "INSERT INTO history (account_id, category, cval, trans_type, comment) values(?1, ?2, ?3, \"-\", ?4)",
            &[&acc.id.unwrap().to_string(), &category.id.unwrap().to_string(), &value.to_string(), &comment],
        )?;
        conn.execute(
            "UPDATE categories
            SET spend = ?2
            WHERE id=?1",
            &[&category.id.unwrap().to_string(), &category.spend.to_string()],
        )?;
        conn.execute(
            "UPDATE accounts
            SET quantity = ?2
            WHERE id=?1",
            &[&acc.id.unwrap().to_string(),&acc.quantity.to_string()],
        )?;

        Ok(())
    }
}
