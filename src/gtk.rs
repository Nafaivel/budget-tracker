// use gtk4 as gtk;
use gtk;
use gtk::prelude::*;
use gtk::{Application, ApplicationWindow, Button, Box, Orientation};

pub fn start() {
    let app = Application::builder()
        .application_id("org.example.HelloWorld")
        .build();

    app.connect_activate(build_ui);

    app.run();
}

fn build_ui(app: &Application){
    // Create button
    let button = Button::builder()
        .label("show")
        .margin_top(12)
        .margin_bottom(12)
        .margin_start(12)
        .margin_end(12)
        .build();
    // Hidden button
    let hidden_button = Button::builder()
        .label("hidie")
        .margin_top(12)
        .margin_bottom(12)
        .margin_start(12)
        .margin_end(12)
        .build();

    let bx = Box::new(Orientation::Vertical, 2);
    bx.append(&button);
    bx.append(&hidden_button);
    hidden_button.hide();
    
    // We create the main window.
    let window = ApplicationWindow::builder()
        .application(app)
        .default_width(320)
        .default_height(200)
        .title("Hello, World!")
        .child(&bx)
        .build();
    
    // Show the window.
    window.show();

    // Connect to "clicked" signal of `button`
    hidden_button.connect_clicked(move |hidden_button| {
        hidden_button.hide();
    });

    button.connect_clicked(move |_| {
        hidden_button.show();
    });

}
