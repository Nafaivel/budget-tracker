use druid::{widget::Controller, Env, UpdateCtx, Widget};

use crate::druid::data::*;

pub struct ListItemController;

impl<W: Widget<ListItem>> Controller<ListItem, W> for ListItemController {
    fn update(
        &mut self,
        child: &mut W,
        ctx: &mut UpdateCtx,
        old_data: &ListItem,
        data: &ListItem,
        env: &Env,
    ) {
        child.update(ctx, old_data, data, env);
    }
}
