use druid::{
    widget::{Axis, Button, Flex, Label, List, Scroll, Split, Tabs, TextBox},
    AppLauncher, Color, Widget, WidgetExt, WindowDesc,
};

use druid::{Data, LocalizedString, RawMods};
use druid::{MenuDesc, MenuItem, Selector};

mod controllers;
mod data;
mod delegate;

use controllers::ListItemController;
use data::*;
use delegate::Delegate;

fn macos_application_menu<T: Data>() -> MenuDesc<T> {
    MenuDesc::new(LocalizedString::new("macos-menu-application-menu"))
        .append(MenuItem::new(
            LocalizedString::new("macos-menu-about-app"),
            Selector::new("druid-builtin.menu-show-about"),
        ))
        .append_separator()
        .append(
            MenuItem::new(
                LocalizedString::new("macos-menu-preferences"),
                Selector::new("druid-builtin.menu-show-preferences"),
            )
            .hotkey(RawMods::Meta, ",")
            .disabled(),
        )
        .append_separator()
        .append(MenuDesc::new(LocalizedString::new("macos-menu-services")))
        .append(
            MenuItem::new(
                LocalizedString::new("macos-menu-hide-app"),
                Selector::new("druid-builtin.menu-hide-application"),
            )
            .hotkey(RawMods::Meta, "h"),
        )
        .append(
            MenuItem::new(
                LocalizedString::new("macos-menu-hide-others"),
                Selector::new("druid-builtin.menu-hide-others"),
            )
            .hotkey(RawMods::AltMeta, "h"),
        )
        .append(
            MenuItem::new(
                LocalizedString::new("macos-menu-show-all"),
                Selector::new("druid-builtin.menu-show-all"),
            )
            .disabled(),
        )
        .append_separator()
        .append(
            MenuItem::new(
                LocalizedString::new("macos-menu-quit-app"),
                Selector::new("druid-builtin.quit-app"),
            )
            .hotkey(RawMods::Meta, "q"),
        )
}

pub fn start() {
    let main_window = WindowDesc::new(build_ui)
        .menu(macos_application_menu())
        .title("BTracker")
        .window_size((400.0, 400.0));

    let conn_path: String = "/home/antasa/development/rust/budget-tracker/base.db".to_string();
    let list_accs = vec![];
    let list_cat = vec![];
    let list_curr = vec![];
    let history = vec![];

    let initial_state = AppState::new(list_accs, list_cat, list_curr, history, conn_path);

    AppLauncher::with_window(main_window)
        .delegate(Delegate {})
        .launch(initial_state)
        .expect("Failed to launch application");
}

fn acc_list_item() -> impl Widget<ListItem> {
    let label = Label::raw().lens(ListItem::text);
    let rm_button = Button::new("rm").on_click(ListItem::acc_click_rm);
    Flex::row()
        .with_flex_child(label, 0.)
        .with_flex_spacer(1.0)
        .with_child(rm_button)
        .background(Color::from_hex_str("0f0f0f").unwrap())
        .controller(ListItemController)
}

fn curr_list_item() -> impl Widget<ListItem> {
    let label = Label::raw().lens(ListItem::text);
    let rm_button = Button::new("rm").on_click(ListItem::curr_click_rm);
    Flex::row()
        .with_flex_child(label, 0.)
        .with_flex_spacer(1.0)
        .with_child(rm_button)
        .background(Color::from_hex_str("0f0f0f").unwrap())
        .controller(ListItemController)
}

fn cat_list_item() -> impl Widget<ListItem> {
    let label = Label::raw().lens(ListItem::text);
    let rm_button = Button::new("rm").on_click(ListItem::cat_click_rm);
    Flex::row()
        .with_flex_child(label, 0.)
        .with_flex_spacer(1.0)
        .with_child(rm_button)
        .background(Color::from_hex_str("0f0f0f").unwrap())
        .controller(ListItemController)
}

fn history_list_item() -> impl Widget<ListItem> {
    let label = Label::raw().lens(ListItem::text);
    Flex::row()
        .with_flex_child(label, 0.)
        .with_flex_spacer(1.0)
        .background(Color::from_hex_str("0f0f0f").unwrap())
        .controller(ListItemController)
}

fn db_op_cr() -> impl Widget<AppState> {
    let db_path_textbox = TextBox::new()
        .with_placeholder("Path to Db")
        .expand_width()
        .lens(AppState::conn_path);
    let db_open_button = Button::new("open").on_click(AppState::click_db_open);
    let db_new_button = Button::new("new").on_click(AppState::click_db_create);
    Flex::row().with_flex_child(db_path_textbox, 1.).with_child(
        Flex::column()
            .with_child(db_open_button)
            .with_child(db_new_button),
    )
}

pub fn build_ui() -> impl Widget<AppState> {
    Split::rows(
        Tabs::new()
            .with_axis(Axis::Vertical)
            .with_tab("db", db_op_cr())
            .with_tab(
                "accounts",
                Scroll::new(
                    Flex::column()
                        .with_child(List::new(acc_list_item).lens(AppState::accounts))
                        .with_child(Button::new("add").on_click(AppState::click_acc_create)),
                )
                .vertical(),
            )
            .with_tab(
                "categories",
                Scroll::new(
                    Flex::column()
                        .with_child(List::new(cat_list_item).lens(AppState::categories))
                        .with_child(
                            Button::new("add"), // .on_click()
                        ),
                )
                .vertical(),
            )
            .with_tab(
                "history",
                Scroll::new(List::new(history_list_item).lens(AppState::history)).vertical(),
            )
            .with_tab(
                "currencies",
                Scroll::new(
                    Flex::column()
                        .with_child(List::new(curr_list_item).lens(AppState::currencies))
                        .with_child(
                            Button::new("add"), // .on_click()
                        ),
                )
                .vertical(),
            ),
        Split::columns(Button::new("+"), Button::new("-")).bar_size(0.),
    )
    .bar_size(0.)
    .split_point(0.9)
    // .draggable(true)
}
