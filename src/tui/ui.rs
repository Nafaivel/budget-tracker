use crossterm::execute;
use tui::{
    backend::Backend,
    layout::{Alignment, Constraint, Direction, Layout, Rect},
    style::{Color, Modifier, Style},
    text::{Span, Spans},
    widgets::{Block, BorderType, Borders, Clear, List, ListItem, Paragraph, Tabs, Wrap},
    Frame,
};

use crate::{tui::{App, AddMode, InputMode, MenuItem}, db::Account};

pub fn ui<B: Backend>(f: &mut Frame<B>, app: &mut App) {
    // TODO: Create add menu
    // TODO: Create history menu
    // TODO: Category menu

    let size = f.size();
    let mut constraints = vec![Constraint::Min(2)];

    let chunks = Layout::default()
        .direction(Direction::Vertical)
        // .margin(1)
        .constraints(constraints.as_ref())
        .split(size);

    match app.selected_menu {
        MenuItem::Summary => f.render_widget(render_summary(&app), chunks[0]),
        MenuItem::Accounts => {
            let accs_chunks = Layout::default()
                .direction(Direction::Horizontal)
                .constraints([Constraint::Length(35), Constraint::Min(1)].as_ref())
                .split(chunks[0]);
            let (left, right) = render_accounts(app);
            f.render_stateful_widget(left, accs_chunks[0], &mut app.accounts_menu.pos);
            f.render_widget(right, accs_chunks[1]);
        }
        // TODO: Add menu
        MenuItem::Add => {
            let mut add_constraints = vec![];

            match app.add_menu.addmode {
                AddMode::Plus | AddMode::Minus => {
                    // account,
                    add_constraints.push(Constraint::Length(1));
                    add_constraints.push(Constraint::Length(3));
                    // category,
                    add_constraints.push(Constraint::Length(1));
                    add_constraints.push(Constraint::Length(3));
                    // trans_type,
                    add_constraints.push(Constraint::Length(1));
                    add_constraints.push(Constraint::Length(3));
                    // cval, cval is amount of spents
                    add_constraints.push(Constraint::Length(1));
                    add_constraints.push(Constraint::Length(3));
                    // comment
                    add_constraints.push(Constraint::Length(1));
                    add_constraints.push(Constraint::Length(3));

                    for (i, rw) in render_add(app).iter().enumerate() {
                        f.render_widget(rw.clone(), add_chunks[i]);
                    }
                }
                AddMode::Account => {}
                AddMode::Category => {}
                AddMode::Currency => {}
                _ => {}
            }

            let add_chunks = Layout::default()
                .direction(Direction::Vertical)
                .constraints(add_constraints)
                .split(chunks[0]);
        }
        _ => {}
    }

    match app.input_mode {
        // TODO: non normal inputmode changes displayed windgets
        _ => {}
    }
}

fn render_summary<'a>(app: &App) -> Paragraph<'a> {
    let mut spans = vec![
        Spans::from(vec![Span::raw("")]),
        Spans::from(vec![Span::raw("Welcome")]),
        Spans::from(vec![Span::raw("")]),
        Spans::from(vec![Span::raw("Here's your summary")]),
        Spans::from(vec![Span::raw("")]),
    ];

    if !app.accounts_menu.accounts.is_empty() {
        for acc in &app.accounts_menu.accounts {
            spans.push(Spans::from(vec![
                // TODO: add some collors
                Span::raw(acc.name.clone()),
                Span::raw(": "),
                Span::raw(acc.quantity.to_string()),
                // TODO: add currency display here
                // Span::raw(),
            ]))
        }
    } else {
        spans.push(Spans::from(vec![Span::raw("Theres no accounts")]))
    }

    let summary = Paragraph::new(spans)
        .alignment(Alignment::Center)
        .wrap(Wrap { trim: true })
        .block(
            Block::default()
                .borders(Borders::ALL)
                .style(Style::default().fg(Color::White))
                .title("Summary")
                .border_type(BorderType::Plain),
        );
    summary
}

fn render_add<'a>(app: &mut App) -> Vec<Paragraph<'a>> {

}

fn render_accounts<'a>(app: &mut App) -> (List<'a>, Paragraph<'a>) {
    let accounts_block = Block::default()
        .borders(Borders::ALL)
        .style(Style::default().fg(Color::White))
        .title("Accounts")
        .border_type(BorderType::Plain);

    let empty_acc = Account::new(None, "None".to_string(), 0, None);

    let acc_list = app.accounts_menu.accounts.clone();

    let mut items: Vec<_> = vec![ListItem::new(Spans::from(Span::styled(empty_acc.name.clone(), Style::default())))];

    let mut selected_acc = empty_acc.clone();

    if !acc_list.is_empty() {
        items.pop();

        for acc in &acc_list {
            items.push(ListItem::new(Spans::from(Span::styled(acc.name.clone(), Style::default()))))
        }

        selected_acc = acc_list
            .get(
                app.accounts_menu
                    .pos
                    .selected()
                    .unwrap()
            )
            .unwrap()
            .clone();
    }

    let list_widget = List::new(items).block(accounts_block).highlight_style(
        Style::default()
            .bg(Color::Gray)
            .fg(Color::Black)
            .add_modifier(Modifier::BOLD)
    );

    let deskription_block = Block::default()
        .borders(Borders::ALL)
        .style(Style::default().fg(Color::White))
        .title("Description")
        .border_type(BorderType::Plain);

    let mut acc_description: Vec<_> = vec![
        Spans::from(Span::styled("name: ".to_string() + &selected_acc.name, Style::default())),
        Spans::from(Span::styled("amount: ".to_string() + &selected_acc.quantity.to_string(), Style::default()))
        // TODO: put currency here
        // TODO: add description of acc here
        // TODO: put last n history entries (n will seted in settings)
    ];

    let acc_description_widget = Paragraph::new(acc_description)
        .wrap(Wrap { trim: true })
        .block(deskription_block);

    (list_widget, acc_description_widget)
}
